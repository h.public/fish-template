import { GameScene } from "./scenes/GameScene";
import { LoadingScene } from "./scenes/LoadingScene";

export class Game extends Core.Game
{
    constructor() { super(); }

    protected retrieveLoadingScene(): IScene { return new LoadingScene(); }
    protected retrieveGameScene(): IScene { return new GameScene(); }
    protected async initPreloader()
    {
        await super.initPreloader();
        this.cetralizeScene(this._loadingUi);
    }

    protected async onResourceLoaded()
    {
        await super.onResourceLoaded();
        this.cetralizeScene(this._scene);
    }

    protected cetralizeScene($scene: IScene): void
    {
        if ($scene.width > AppData.content.width)
        {
            $scene.x = (AppData.content.width - $scene.width) / 2;
        }
    }
}