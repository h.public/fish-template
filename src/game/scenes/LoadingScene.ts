export class LoadingScene extends Core.Scene
{
    constructor() { super(); }

    protected _loader: ILoader;

    protected retrieveSceneName(): string { return "res/scenes/LoadingScene.json"; }
    public init(): void
    {
        const loader: ILoader = this._loader = new Core.Loader();
        loader.once(LoaderEvent.LOAD_COMPLETE, this, this.onResourceLoaded);
        loader.on(LoaderEvent.LOAD_PROGRESS, this, this.onLoadProgress);
        loader.loadGroup("game");
    }

    protected onLoadProgress($e): void
    {
        // $e.progress %
    }

    protected onResourceLoaded(): void
    {
        this.event(Laya.Event.COMPLETE);
    }

    public destroy($destroyChild?: boolean): void
    {
        this._loader.off(LoaderEvent.LOAD_COMPLETE, this, this.onResourceLoaded);
        this._loader.off(LoaderEvent.LOAD_PROGRESS, this, this.onLoadProgress);
        super.destroy($destroyChild);
    }
}