import { Game } from "./game/Game";
import { LayaApp } from "./LayaApp";

class Main extends LayaApp
{
  constructor() { super(); }

  protected retrieveGame(): IGame { return new Game(); }
}

new Main();
