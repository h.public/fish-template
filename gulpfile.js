const global =
  require('child_process').execSync('npm root -g').toString().trim() + '/';
const gulp = require('gulp');
const { rollup } = require(global + 'rollup');
const typescript = require(global + 'rollup-plugin-typescript2'); //typescript2 plugin
const replace = require(global + '@rollup/plugin-replace');
const { terser } = require(global + 'rollup-plugin-terser');

gulp.task('compile', () => {
  const target = process.argv[4] || 'dev';
  return rollup({
    input: './src/Main.ts',
    plugins: [
      typescript({
        tsconfig: './tsconfig.json',
      }),
      replace({
        DEBUG: target === 'dev',
        SIT: target === 'sit',
        UAT: target === 'uat',
        PRODUCTION: target === 'prod',
        BUILD_INCREMENT: new Date().getTime(),
      }),
      (target === 'uat' || target === 'prod') && terser(),
    ],
    treeshake: true,
  }).then((bundle) => {
    const target = process.argv[4] || 'dev';
    const path = './bin/js/';
    return bundle.write({
      file: `${path}/bundle.js`,
      format: 'iife',
      // format: 'module',
      // name: 'game',
      sourcemap: false,
    });
  });
});

// compile all sound into 1 sound sprite
const exec = require('child_process').exec;
gulp.task('audiosprite', function () {
  const res = './laya/assets/sound/**/*';
  const output = './bin/res/assets/sound/audiosprite';
  const fileTypes = 'mp3';
  const format = 'howler2';

  return exec(
    `audiosprite -o ${output} -e ${fileTypes} -f ${format} ${res}.wav ${res}.mp3 ${res}.ogg`
  );
});

/// Native StandAlone
// refresh android resources
gulp.task('refreshres', function () {
  const platform = 'android_studio';
  const path = 'bin_native';
  return exec(`layanative2 refreshres -p ${platform} --path ${path}`);
});

gulp.task('compile_native', gulp.series('compile', 'refreshres'));
